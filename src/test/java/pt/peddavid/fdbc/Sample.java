package pt.peddavid.fdbc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.MSSQLServerContainer;

import java.sql.SQLException;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;

public class Sample{
    private static ConnectionService service;

    public static MSSQLServerContainer container = new MSSQLServerContainer();

    @BeforeAll
    public static void setUp() throws SQLException {
        container.start();
        service = new MSSQLService(container.getJdbcUrl(), container.getUsername(), container.getPassword());
    }


    public void createTestTable(String name) throws SQLException {
        service.update(
                "create Table " + name + "(" +
                    "id int IDENTITY(1,1) primary key, " +
                    "name varchar(50) NOT NULL, " +
                ")")
                .execute();
    }

    @Test
    public void createTable() throws SQLException {
        createTestTable("dbo.CreateTable");
        Assertions.assertTrue(service.query("select * from dbo.CreateTable").collect(Collectors.toList()).isEmpty());
    }

    @Test
    public void insert() throws SQLException {
        createTestTable("dbo.InsertTable");
        service.update("insert into dbo.InsertTable(name) values ('test')").execute();
        assertFalse(service.query("select * from dbo.InsertTable").collect(Collectors.toList()).isEmpty());
    }
}
