package pt.peddavid.fdbc;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import pt.peddavid.fdbc.connections.BaseConnection;

import java.sql.Connection;
import java.sql.SQLException;

public class MSSQLService implements ConnectionService {
    private final SQLServerDataSource src = new SQLServerDataSource();

    public MSSQLService(String url, String user, String password) throws SQLException {
        src.setURL(url);
        src.setUser(user);
        src.setPassword(password);
        Connection con = src.getConnection();
        con.close();
    }

    @Override
    public BaseConnection getConnection() throws SQLException {
        return new BaseConnection(src.getConnection());
    }
}
